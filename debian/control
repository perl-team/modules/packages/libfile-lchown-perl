Source: libfile-lchown-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libextutils-cchecker-perl,
               libtest-pod-perl,
               perl-xs-dev,
               perl:native,
               libmodule-build-perl
Standards-Version: 3.9.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libfile-lchown-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libfile-lchown-perl.git
Homepage: https://metacpan.org/release/File-lchown

Package: libfile-lchown-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Description: module to modify attributes of symlinks without dereferencing them
 The regular chown system call will dereference a symlink and apply ownership
 changes to the file at which it points. Some OSes provide system calls that
 do not dereference a symlink but instead apply their changes directly to the
 named path, even if that path is a symlink (in much the same way that lstat
 will return attributes of a symlink rather than the file at which it points).
 .
 File::lchown provides a wrapper around those system calls.
